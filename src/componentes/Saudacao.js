import React, { Component } from 'react';
import { setInterval } from 'timers';


export default class Saudacao extends Component {
   
   state = {
      tipo: "falar",
      nome: "leandro"
   }

   setTipo(e) {
      let oo = 1
      setInterval( () =>{
         this.setState({tipo: oo++})
      },1100)
      this.setState({tipo: e.target.value})  
   }
   render(){
      const {tipo, nome} = this.state
            
      return(
         <div>
            <h1 id='testandoalfa'> {tipo} {nome} </h1>
            
            <hr />
            
            <input type="text" placeholder="tipo?.." value={tipo} onChange={e => this.setTipo(e)} />
            <input type="text" placeholder="qual nome?.." value={nome} />
            
         </div>   
      )
   }
}