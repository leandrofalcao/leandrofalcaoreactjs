import React, { Fragment, Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Primeiro from './componentes/Primeiro';

import BomDia from './componentes/BomDia'
import Boa from './componentes/Multiplos';
//import BoaNoite1 from './componentes/multiplos.BoaNoite'
import Saudacao from './componentes/Saudacao';

import Multiplos from './componentes/Multiplos';

import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Fragment>
    <Saudacao tipo='bom dia' nome='leandro' />
    <Multiplos.BoaNoite />
  </Fragment>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
